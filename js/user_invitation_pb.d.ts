// package: octopulse
// file: user_invitation.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class UserInvitation extends jspb.Message { 
    getType(): UserInvitation.Type;
    setType(value: UserInvitation.Type): UserInvitation;
    getInvitationid(): number;
    setInvitationid(value: number): UserInvitation;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): UserInvitation.AsObject;
    static toObject(includeInstance: boolean, msg: UserInvitation): UserInvitation.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: UserInvitation, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): UserInvitation;
    static deserializeBinaryFromReader(message: UserInvitation, reader: jspb.BinaryReader): UserInvitation;
}

export namespace UserInvitation {
    export type AsObject = {
        type: UserInvitation.Type,
        invitationid: number,
    }

    export enum Type {
    UNKNOWN = 0,
    SUBMITTED = 1,
    ACCEPTED = 2,
    REFUSED = 3,
    UPDATED = 4,
    }

}
