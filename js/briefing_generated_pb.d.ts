// package: octopulse
// file: briefing_generated.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class BriefingGenerated extends jspb.Message { 
    getType(): BriefingGenerated.Type;
    setType(value: BriefingGenerated.Type): BriefingGenerated;
    getOrderid(): number;
    setOrderid(value: number): BriefingGenerated;
    getAuditid(): number;
    setAuditid(value: number): BriefingGenerated;
    getGoogledriveurl(): string;
    setGoogledriveurl(value: string): BriefingGenerated;
    getAdscampaignid(): number;
    setAdscampaignid(value: number): BriefingGenerated;
    getBudget(): number;
    setBudget(value: number): BriefingGenerated;
    getFormula(): string;
    setFormula(value: string): BriefingGenerated;
    getBloburl(): string;
    setBloburl(value: string): BriefingGenerated;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): BriefingGenerated.AsObject;
    static toObject(includeInstance: boolean, msg: BriefingGenerated): BriefingGenerated.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: BriefingGenerated, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): BriefingGenerated;
    static deserializeBinaryFromReader(message: BriefingGenerated, reader: jspb.BinaryReader): BriefingGenerated;
}

export namespace BriefingGenerated {
    export type AsObject = {
        type: BriefingGenerated.Type,
        orderid: number,
        auditid: number,
        googledriveurl: string,
        adscampaignid: number,
        budget: number,
        formula: string,
        bloburl: string,
    }

    export enum Type {
    UNKNOWKN = 0,
    SEO_EXPORTED = 1,
    SEO_GENERATED = 2,
    SEA_GENERATED = 3,
    NETLINKNG_GENERATED = 4,
    SEA_BUDGET_UPDATED = 5,
    RANKING_GENERATED = 6,
    LOCAL_SEO_GENERATED = 7,
    }

}
