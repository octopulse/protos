// package: octopulse
// file: netlinking.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Netlinking extends jspb.Message { 
    getType(): Netlinking.Type;
    setType(value: Netlinking.Type): Netlinking;
    getOrderid(): number;
    setOrderid(value: number): Netlinking;
    getCampaignid(): number;
    setCampaignid(value: number): Netlinking;
    getPublicationid(): number;
    setPublicationid(value: number): Netlinking;
    getConfigurationid(): number;
    setConfigurationid(value: number): Netlinking;
    getComment(): string;
    setComment(value: string): Netlinking;
    getNewcampaignduration(): number;
    setNewcampaignduration(value: number): Netlinking;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Netlinking.AsObject;
    static toObject(includeInstance: boolean, msg: Netlinking): Netlinking.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Netlinking, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Netlinking;
    static deserializeBinaryFromReader(message: Netlinking, reader: jspb.BinaryReader): Netlinking;
}

export namespace Netlinking {
    export type AsObject = {
        type: Netlinking.Type,
        orderid: number,
        campaignid: number,
        publicationid: number,
        configurationid: number,
        comment: string,
        newcampaignduration: number,
    }

    export enum Type {
    UNKNOWN_STATUS = 0,
    ORDER_REQUESTED = 1,
    ORDER_PROCESSED = 2,
    PUBLICATION_EDIT_REQUESTED = 3,
    CONFIGURATION_EDIT_REQUESTED = 4,
    EXTEND_CAMPAIGN_DURATION = 5,
    REPORTING_ARTICLES_PUBLISHED = 6,
    }

}
