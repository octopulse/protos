// package: octopulse
// file: authentication_request.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class AuthenticationRequest extends jspb.Message { 
    getUserid(): number;
    setUserid(value: number): AuthenticationRequest;
    getType(): AuthenticationRequest.Type;
    setType(value: AuthenticationRequest.Type): AuthenticationRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): AuthenticationRequest.AsObject;
    static toObject(includeInstance: boolean, msg: AuthenticationRequest): AuthenticationRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: AuthenticationRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): AuthenticationRequest;
    static deserializeBinaryFromReader(message: AuthenticationRequest, reader: jspb.BinaryReader): AuthenticationRequest;
}

export namespace AuthenticationRequest {
    export type AsObject = {
        userid: number,
        type: AuthenticationRequest.Type,
    }

    export enum Type {
    UNKNOWKN = 0,
    RESET_PASSWORD = 1,
    UNVERIFIED_USER = 2,
    }

}
