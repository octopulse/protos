// package: octopulse
// file: subscription.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Subscription extends jspb.Message { 
    getAccountid(): number;
    setAccountid(value: number): Subscription;
    getStatus(): Subscription.Status;
    setStatus(value: Subscription.Status): Subscription;
    getEndsubdate(): string;
    setEndsubdate(value: string): Subscription;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Subscription.AsObject;
    static toObject(includeInstance: boolean, msg: Subscription): Subscription.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Subscription, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Subscription;
    static deserializeBinaryFromReader(message: Subscription, reader: jspb.BinaryReader): Subscription;
}

export namespace Subscription {
    export type AsObject = {
        accountid: number,
        status: Subscription.Status,
        endsubdate: string,
    }

    export enum Status {
    UNKNOWN = 0,
    SUBSCRIBED = 1,
    CANCELED = 2,
    }

}
