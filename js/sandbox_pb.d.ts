// package: octopulse
// file: sandbox.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Sandbox extends jspb.Message { 
    getId(): number;
    setId(value: number): Sandbox;
    getAction(): Sandbox.Action;
    setAction(value: Sandbox.Action): Sandbox;
    clearImagesList(): void;
    getImagesList(): Array<Sandbox.Image>;
    setImagesList(value: Array<Sandbox.Image>): Sandbox;
    addImages(value: Sandbox.Image, index?: number): Sandbox.Image;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Sandbox.AsObject;
    static toObject(includeInstance: boolean, msg: Sandbox): Sandbox.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Sandbox, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Sandbox;
    static deserializeBinaryFromReader(message: Sandbox, reader: jspb.BinaryReader): Sandbox;
}

export namespace Sandbox {
    export type AsObject = {
        id: number,
        action: Sandbox.Action,
        imagesList: Array<Sandbox.Image>,
    }

    export enum Action {
    DEPLOY = 0,
    DELETE = 1,
    RESET = 2,
    }

    export enum Image {
    UNKNOWN = 0,
    MYSQL_8_0_19 = 1,
    MYSQL_5_7_29 = 2,
    MYSQL_5_6_47 = 3,
    WORDPRESS_5_3_2 = 4,
    }

}
