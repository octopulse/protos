// package: octopulse
// file: ranking.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Ranking extends jspb.Message { 
    getId(): number;
    setId(value: number): Ranking;
    getPeriodicity(): Ranking.Periodicity;
    setPeriodicity(value: Ranking.Periodicity): Ranking;
    getType(): Ranking.Type;
    setType(value: Ranking.Type): Ranking;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Ranking.AsObject;
    static toObject(includeInstance: boolean, msg: Ranking): Ranking.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Ranking, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Ranking;
    static deserializeBinaryFromReader(message: Ranking, reader: jspb.BinaryReader): Ranking;
}

export namespace Ranking {
    export type AsObject = {
        id: number,
        periodicity: Ranking.Periodicity,
        type: Ranking.Type,
    }

    export enum Periodicity {
    UNKNOWN_PERIODICITY = 0,
    DAILY = 1,
    WEEKLY = 2,
    MONTHLY = 3,
    }

    export enum Type {
    UNKNOWN_STATUS = 0,
    REPORTING = 1,
    }

}
