// package: octopulse
// file: sea_form_request.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class SeaFormRequest extends jspb.Message { 
    getEmail(): string;
    setEmail(value: string): SeaFormRequest;
    getName(): string;
    setName(value: string): SeaFormRequest;
    getBudget(): number;
    setBudget(value: number): SeaFormRequest;
    getWebsiteurl(): string;
    setWebsiteurl(value: string): SeaFormRequest;
    getInvoicepaymenturl(): string;
    setInvoicepaymenturl(value: string): SeaFormRequest;
    getInvoicepdfurl(): string;
    setInvoicepdfurl(value: string): SeaFormRequest;
    getQuoteid(): string;
    setQuoteid(value: string): SeaFormRequest;
    getQuotepdf(): string;
    setQuotepdf(value: string): SeaFormRequest;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): SeaFormRequest.AsObject;
    static toObject(includeInstance: boolean, msg: SeaFormRequest): SeaFormRequest.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: SeaFormRequest, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): SeaFormRequest;
    static deserializeBinaryFromReader(message: SeaFormRequest, reader: jspb.BinaryReader): SeaFormRequest;
}

export namespace SeaFormRequest {
    export type AsObject = {
        email: string,
        name: string,
        budget: number,
        websiteurl: string,
        invoicepaymenturl: string,
        invoicepdfurl: string,
        quoteid: string,
        quotepdf: string,
    }
}
