// package: octopulse
// file: registration.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Registration extends jspb.Message { 
    getUserid(): number;
    setUserid(value: number): Registration;
    getEmail(): string;
    setEmail(value: string): Registration;
    getFirstname(): string;
    setFirstname(value: string): Registration;
    getPhone(): string;
    setPhone(value: string): Registration;
    getLastname(): string;
    setLastname(value: string): Registration;
    getWebsite(): string;
    setWebsite(value: string): Registration;
    getCompanyname(): string;
    setCompanyname(value: string): Registration;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Registration.AsObject;
    static toObject(includeInstance: boolean, msg: Registration): Registration.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Registration, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Registration;
    static deserializeBinaryFromReader(message: Registration, reader: jspb.BinaryReader): Registration;
}

export namespace Registration {
    export type AsObject = {
        userid: number,
        email: string,
        firstname: string,
        phone: string,
        lastname: string,
        website: string,
        companyname: string,
    }
}
