// package: octopulse
// file: audit.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Audit extends jspb.Message { 
    getId(): number;
    setId(value: number): Audit;
    getCrawlid(): string;
    setCrawlid(value: string): Audit;
    getDepth(): number;
    setDepth(value: number): Audit;
    getMaxurls(): number;
    setMaxurls(value: number): Audit;
    getSpeed(): number;
    setSpeed(value: number): Audit;
    getWebsite(): string;
    setWebsite(value: string): Audit;
    getDisableemail(): boolean;
    setDisableemail(value: boolean): Audit;

    hasVersion(): boolean;
    clearVersion(): void;
    getVersion(): Audit.Version | undefined;
    setVersion(value?: Audit.Version): Audit;
    getStatus(): Audit.Status;
    setStatus(value: Audit.Status): Audit;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Audit.AsObject;
    static toObject(includeInstance: boolean, msg: Audit): Audit.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Audit, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Audit;
    static deserializeBinaryFromReader(message: Audit, reader: jspb.BinaryReader): Audit;
}

export namespace Audit {
    export type AsObject = {
        id: number,
        crawlid: string,
        depth: number,
        maxurls: number,
        speed: number,
        website: string,
        disableemail: boolean,
        version?: Audit.Version.AsObject,
        status: Audit.Status,
    }


    export class Version extends jspb.Message { 
        getCrawl(): string;
        setCrawl(value: string): Version;
        getBatch(): string;
        setBatch(value: string): Version;

        serializeBinary(): Uint8Array;
        toObject(includeInstance?: boolean): Version.AsObject;
        static toObject(includeInstance: boolean, msg: Version): Version.AsObject;
        static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
        static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
        static serializeBinaryToWriter(message: Version, writer: jspb.BinaryWriter): void;
        static deserializeBinary(bytes: Uint8Array): Version;
        static deserializeBinaryFromReader(message: Version, reader: jspb.BinaryReader): Version;
    }

    export namespace Version {
        export type AsObject = {
            crawl: string,
            batch: string,
        }
    }


    export enum Status {
    UNKNOWN_STATUS = 0,
    STARTED = 1,
    FINISHED = 2,
    CRASHED = 3,
    }

}
