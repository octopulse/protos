// package: octopulse
// file: order.proto

/* tslint:disable */
/* eslint-disable */

import * as jspb from "google-protobuf";

export class Order extends jspb.Message { 
    getType(): Order.Type;
    setType(value: Order.Type): Order;
    getOrderid(): number;
    setOrderid(value: number): Order;
    getAuditid(): number;
    setAuditid(value: number): Order;
    getInfo(): string;
    setInfo(value: string): Order;

    serializeBinary(): Uint8Array;
    toObject(includeInstance?: boolean): Order.AsObject;
    static toObject(includeInstance: boolean, msg: Order): Order.AsObject;
    static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
    static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
    static serializeBinaryToWriter(message: Order, writer: jspb.BinaryWriter): void;
    static deserializeBinary(bytes: Uint8Array): Order;
    static deserializeBinaryFromReader(message: Order, reader: jspb.BinaryReader): Order;
}

export namespace Order {
    export type AsObject = {
        type: Order.Type,
        orderid: number,
        auditid: number,
        info: string,
    }

    export enum Type {
    UNKNOWKN = 0,
    SEO = 1,
    SEA = 2,
    NETLINKING = 3,
    RANKING = 4,
    ABANDONED_CART = 5,
    SEO_EXPORT = 6,
    LOCAL_SEO = 7,
    CONTACT = 8,
    }

}
