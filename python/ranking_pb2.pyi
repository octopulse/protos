from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Ranking(_message.Message):
    __slots__ = ["id", "periodicity", "type"]
    class Periodicity(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    class Type(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    DAILY: Ranking.Periodicity
    ID_FIELD_NUMBER: _ClassVar[int]
    MONTHLY: Ranking.Periodicity
    PERIODICITY_FIELD_NUMBER: _ClassVar[int]
    REPORTING: Ranking.Type
    TYPE_FIELD_NUMBER: _ClassVar[int]
    UNKNOWN_PERIODICITY: Ranking.Periodicity
    UNKNOWN_STATUS: Ranking.Type
    WEEKLY: Ranking.Periodicity
    id: int
    periodicity: Ranking.Periodicity
    type: Ranking.Type
    def __init__(self, id: _Optional[int] = ..., periodicity: _Optional[_Union[Ranking.Periodicity, str]] = ..., type: _Optional[_Union[Ranking.Type, str]] = ...) -> None: ...
