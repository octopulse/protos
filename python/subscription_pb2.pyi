from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Subscription(_message.Message):
    __slots__ = ["accountId", "endSubDate", "status"]
    class Status(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    ACCOUNTID_FIELD_NUMBER: _ClassVar[int]
    CANCELED: Subscription.Status
    ENDSUBDATE_FIELD_NUMBER: _ClassVar[int]
    STATUS_FIELD_NUMBER: _ClassVar[int]
    SUBSCRIBED: Subscription.Status
    UNKNOWN: Subscription.Status
    accountId: int
    endSubDate: str
    status: Subscription.Status
    def __init__(self, accountId: _Optional[int] = ..., status: _Optional[_Union[Subscription.Status, str]] = ..., endSubDate: _Optional[str] = ...) -> None: ...
