from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional

DESCRIPTOR: _descriptor.FileDescriptor

class SeaFormRequest(_message.Message):
    __slots__ = ["budget", "email", "invoicePaymentUrl", "invoicePdfUrl", "name", "quoteId", "quotePdf", "websiteUrl"]
    BUDGET_FIELD_NUMBER: _ClassVar[int]
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    INVOICEPAYMENTURL_FIELD_NUMBER: _ClassVar[int]
    INVOICEPDFURL_FIELD_NUMBER: _ClassVar[int]
    NAME_FIELD_NUMBER: _ClassVar[int]
    QUOTEID_FIELD_NUMBER: _ClassVar[int]
    QUOTEPDF_FIELD_NUMBER: _ClassVar[int]
    WEBSITEURL_FIELD_NUMBER: _ClassVar[int]
    budget: int
    email: str
    invoicePaymentUrl: str
    invoicePdfUrl: str
    name: str
    quoteId: str
    quotePdf: str
    websiteUrl: str
    def __init__(self, email: _Optional[str] = ..., name: _Optional[str] = ..., budget: _Optional[int] = ..., websiteUrl: _Optional[str] = ..., invoicePaymentUrl: _Optional[str] = ..., invoicePdfUrl: _Optional[str] = ..., quoteId: _Optional[str] = ..., quotePdf: _Optional[str] = ...) -> None: ...
