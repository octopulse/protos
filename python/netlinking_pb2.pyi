from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Netlinking(_message.Message):
    __slots__ = ["campaignId", "comment", "configurationId", "newCampaignDuration", "orderId", "publicationId", "type"]
    class Type(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    CAMPAIGNID_FIELD_NUMBER: _ClassVar[int]
    COMMENT_FIELD_NUMBER: _ClassVar[int]
    CONFIGURATIONID_FIELD_NUMBER: _ClassVar[int]
    CONFIGURATION_EDIT_REQUESTED: Netlinking.Type
    EXTEND_CAMPAIGN_DURATION: Netlinking.Type
    NEWCAMPAIGNDURATION_FIELD_NUMBER: _ClassVar[int]
    ORDERID_FIELD_NUMBER: _ClassVar[int]
    ORDER_PROCESSED: Netlinking.Type
    ORDER_REQUESTED: Netlinking.Type
    PUBLICATIONID_FIELD_NUMBER: _ClassVar[int]
    PUBLICATION_EDIT_REQUESTED: Netlinking.Type
    REPORTING_ARTICLES_PUBLISHED: Netlinking.Type
    TYPE_FIELD_NUMBER: _ClassVar[int]
    UNKNOWN_STATUS: Netlinking.Type
    campaignId: int
    comment: str
    configurationId: int
    newCampaignDuration: int
    orderId: int
    publicationId: int
    type: Netlinking.Type
    def __init__(self, type: _Optional[_Union[Netlinking.Type, str]] = ..., orderId: _Optional[int] = ..., campaignId: _Optional[int] = ..., publicationId: _Optional[int] = ..., configurationId: _Optional[int] = ..., comment: _Optional[str] = ..., newCampaignDuration: _Optional[int] = ...) -> None: ...
