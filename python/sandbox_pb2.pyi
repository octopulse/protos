from google.protobuf.internal import containers as _containers
from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Sandbox(_message.Message):
    __slots__ = ["action", "id", "images"]
    class Action(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    class Image(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    ACTION_FIELD_NUMBER: _ClassVar[int]
    DELETE: Sandbox.Action
    DEPLOY: Sandbox.Action
    ID_FIELD_NUMBER: _ClassVar[int]
    IMAGES_FIELD_NUMBER: _ClassVar[int]
    MYSQL_5_6_47: Sandbox.Image
    MYSQL_5_7_29: Sandbox.Image
    MYSQL_8_0_19: Sandbox.Image
    RESET: Sandbox.Action
    UNKNOWN: Sandbox.Image
    WORDPRESS_5_3_2: Sandbox.Image
    action: Sandbox.Action
    id: int
    images: _containers.RepeatedScalarFieldContainer[Sandbox.Image]
    def __init__(self, id: _Optional[int] = ..., action: _Optional[_Union[Sandbox.Action, str]] = ..., images: _Optional[_Iterable[_Union[Sandbox.Image, str]]] = ...) -> None: ...
