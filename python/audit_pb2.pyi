from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Audit(_message.Message):
    __slots__ = ["crawlId", "depth", "disableEmail", "id", "maxUrls", "speed", "status", "version", "website"]
    class Status(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    class Version(_message.Message):
        __slots__ = ["batch", "crawl"]
        BATCH_FIELD_NUMBER: _ClassVar[int]
        CRAWL_FIELD_NUMBER: _ClassVar[int]
        batch: str
        crawl: str
        def __init__(self, crawl: _Optional[str] = ..., batch: _Optional[str] = ...) -> None: ...
    CRASHED: Audit.Status
    CRAWLID_FIELD_NUMBER: _ClassVar[int]
    DEPTH_FIELD_NUMBER: _ClassVar[int]
    DISABLEEMAIL_FIELD_NUMBER: _ClassVar[int]
    FINISHED: Audit.Status
    ID_FIELD_NUMBER: _ClassVar[int]
    MAXURLS_FIELD_NUMBER: _ClassVar[int]
    SPEED_FIELD_NUMBER: _ClassVar[int]
    STARTED: Audit.Status
    STATUS_FIELD_NUMBER: _ClassVar[int]
    UNKNOWN_STATUS: Audit.Status
    VERSION_FIELD_NUMBER: _ClassVar[int]
    WEBSITE_FIELD_NUMBER: _ClassVar[int]
    crawlId: str
    depth: int
    disableEmail: bool
    id: int
    maxUrls: int
    speed: int
    status: Audit.Status
    version: Audit.Version
    website: str
    def __init__(self, id: _Optional[int] = ..., crawlId: _Optional[str] = ..., depth: _Optional[int] = ..., maxUrls: _Optional[int] = ..., speed: _Optional[int] = ..., website: _Optional[str] = ..., disableEmail: bool = ..., version: _Optional[_Union[Audit.Version, _Mapping]] = ..., status: _Optional[_Union[Audit.Status, str]] = ...) -> None: ...
