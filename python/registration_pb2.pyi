from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional

DESCRIPTOR: _descriptor.FileDescriptor

class Registration(_message.Message):
    __slots__ = ["companyName", "email", "firstName", "lastName", "phone", "userId", "website"]
    COMPANYNAME_FIELD_NUMBER: _ClassVar[int]
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    FIRSTNAME_FIELD_NUMBER: _ClassVar[int]
    LASTNAME_FIELD_NUMBER: _ClassVar[int]
    PHONE_FIELD_NUMBER: _ClassVar[int]
    USERID_FIELD_NUMBER: _ClassVar[int]
    WEBSITE_FIELD_NUMBER: _ClassVar[int]
    companyName: str
    email: str
    firstName: str
    lastName: str
    phone: str
    userId: int
    website: str
    def __init__(self, userId: _Optional[int] = ..., email: _Optional[str] = ..., firstName: _Optional[str] = ..., phone: _Optional[str] = ..., lastName: _Optional[str] = ..., website: _Optional[str] = ..., companyName: _Optional[str] = ...) -> None: ...
