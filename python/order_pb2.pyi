from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class Order(_message.Message):
    __slots__ = ["auditId", "info", "orderId", "type"]
    class Type(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    ABANDONED_CART: Order.Type
    AUDITID_FIELD_NUMBER: _ClassVar[int]
    CONTACT: Order.Type
    INFO_FIELD_NUMBER: _ClassVar[int]
    LOCAL_SEO: Order.Type
    NETLINKING: Order.Type
    ORDERID_FIELD_NUMBER: _ClassVar[int]
    RANKING: Order.Type
    SEA: Order.Type
    SEO: Order.Type
    SEO_EXPORT: Order.Type
    TYPE_FIELD_NUMBER: _ClassVar[int]
    UNKNOWKN: Order.Type
    auditId: int
    info: str
    orderId: int
    type: Order.Type
    def __init__(self, type: _Optional[_Union[Order.Type, str]] = ..., orderId: _Optional[int] = ..., auditId: _Optional[int] = ..., info: _Optional[str] = ...) -> None: ...
