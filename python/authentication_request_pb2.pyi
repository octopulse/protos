from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class AuthenticationRequest(_message.Message):
    __slots__ = ["type", "userId"]
    class Type(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    RESET_PASSWORD: AuthenticationRequest.Type
    TYPE_FIELD_NUMBER: _ClassVar[int]
    UNKNOWKN: AuthenticationRequest.Type
    UNVERIFIED_USER: AuthenticationRequest.Type
    USERID_FIELD_NUMBER: _ClassVar[int]
    type: AuthenticationRequest.Type
    userId: int
    def __init__(self, userId: _Optional[int] = ..., type: _Optional[_Union[AuthenticationRequest.Type, str]] = ...) -> None: ...
