from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class BriefingGenerated(_message.Message):
    __slots__ = ["adsCampaignId", "auditId", "blobUrl", "budget", "formula", "googleDriveUrl", "orderId", "type"]
    class Type(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    ADSCAMPAIGNID_FIELD_NUMBER: _ClassVar[int]
    AUDITID_FIELD_NUMBER: _ClassVar[int]
    BLOBURL_FIELD_NUMBER: _ClassVar[int]
    BUDGET_FIELD_NUMBER: _ClassVar[int]
    FORMULA_FIELD_NUMBER: _ClassVar[int]
    GOOGLEDRIVEURL_FIELD_NUMBER: _ClassVar[int]
    LOCAL_SEO_GENERATED: BriefingGenerated.Type
    NETLINKNG_GENERATED: BriefingGenerated.Type
    ORDERID_FIELD_NUMBER: _ClassVar[int]
    RANKING_GENERATED: BriefingGenerated.Type
    SEA_BUDGET_UPDATED: BriefingGenerated.Type
    SEA_GENERATED: BriefingGenerated.Type
    SEO_EXPORTED: BriefingGenerated.Type
    SEO_GENERATED: BriefingGenerated.Type
    TYPE_FIELD_NUMBER: _ClassVar[int]
    UNKNOWKN: BriefingGenerated.Type
    adsCampaignId: int
    auditId: int
    blobUrl: str
    budget: int
    formula: str
    googleDriveUrl: str
    orderId: int
    type: BriefingGenerated.Type
    def __init__(self, type: _Optional[_Union[BriefingGenerated.Type, str]] = ..., orderId: _Optional[int] = ..., auditId: _Optional[int] = ..., googleDriveUrl: _Optional[str] = ..., adsCampaignId: _Optional[int] = ..., budget: _Optional[int] = ..., formula: _Optional[str] = ..., blobUrl: _Optional[str] = ...) -> None: ...
