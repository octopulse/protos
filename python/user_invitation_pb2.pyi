from google.protobuf.internal import enum_type_wrapper as _enum_type_wrapper
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class UserInvitation(_message.Message):
    __slots__ = ["invitationId", "type"]
    class Type(int, metaclass=_enum_type_wrapper.EnumTypeWrapper):
        __slots__ = []
    ACCEPTED: UserInvitation.Type
    INVITATIONID_FIELD_NUMBER: _ClassVar[int]
    REFUSED: UserInvitation.Type
    SUBMITTED: UserInvitation.Type
    TYPE_FIELD_NUMBER: _ClassVar[int]
    UNKNOWN: UserInvitation.Type
    UPDATED: UserInvitation.Type
    invitationId: int
    type: UserInvitation.Type
    def __init__(self, type: _Optional[_Union[UserInvitation.Type, str]] = ..., invitationId: _Optional[int] = ...) -> None: ...
