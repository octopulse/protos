import setuptools


setuptools.setup(
    version="0.1.0",
    name="protos",
    author="Octopulse",
    author_email="contact@octopulse.io",
    description="Protos used for pubsub messages",
    url="https://gitlab.com/octopulse/protos/",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
)