// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.28.1
// 	protoc        v3.20.3
// source: sandbox.proto

package __

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

// Action requested on the sandbox (default is DEPLOY)
type Sandbox_Action int32

const (
	Sandbox_DEPLOY Sandbox_Action = 0
	Sandbox_DELETE Sandbox_Action = 1
	Sandbox_RESET  Sandbox_Action = 2
)

// Enum value maps for Sandbox_Action.
var (
	Sandbox_Action_name = map[int32]string{
		0: "DEPLOY",
		1: "DELETE",
		2: "RESET",
	}
	Sandbox_Action_value = map[string]int32{
		"DEPLOY": 0,
		"DELETE": 1,
		"RESET":  2,
	}
)

func (x Sandbox_Action) Enum() *Sandbox_Action {
	p := new(Sandbox_Action)
	*p = x
	return p
}

func (x Sandbox_Action) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Sandbox_Action) Descriptor() protoreflect.EnumDescriptor {
	return file_sandbox_proto_enumTypes[0].Descriptor()
}

func (Sandbox_Action) Type() protoreflect.EnumType {
	return &file_sandbox_proto_enumTypes[0]
}

func (x Sandbox_Action) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Sandbox_Action.Descriptor instead.
func (Sandbox_Action) EnumDescriptor() ([]byte, []int) {
	return file_sandbox_proto_rawDescGZIP(), []int{0, 0}
}

// List of images which should be deployed within the sandbox
// Only useful with action DEPLOY
type Sandbox_Image int32

const (
	Sandbox_UNKNOWN         Sandbox_Image = 0
	Sandbox_MYSQL_8_0_19    Sandbox_Image = 1
	Sandbox_MYSQL_5_7_29    Sandbox_Image = 2
	Sandbox_MYSQL_5_6_47    Sandbox_Image = 3
	Sandbox_WORDPRESS_5_3_2 Sandbox_Image = 4
)

// Enum value maps for Sandbox_Image.
var (
	Sandbox_Image_name = map[int32]string{
		0: "UNKNOWN",
		1: "MYSQL_8_0_19",
		2: "MYSQL_5_7_29",
		3: "MYSQL_5_6_47",
		4: "WORDPRESS_5_3_2",
	}
	Sandbox_Image_value = map[string]int32{
		"UNKNOWN":         0,
		"MYSQL_8_0_19":    1,
		"MYSQL_5_7_29":    2,
		"MYSQL_5_6_47":    3,
		"WORDPRESS_5_3_2": 4,
	}
)

func (x Sandbox_Image) Enum() *Sandbox_Image {
	p := new(Sandbox_Image)
	*p = x
	return p
}

func (x Sandbox_Image) String() string {
	return protoimpl.X.EnumStringOf(x.Descriptor(), protoreflect.EnumNumber(x))
}

func (Sandbox_Image) Descriptor() protoreflect.EnumDescriptor {
	return file_sandbox_proto_enumTypes[1].Descriptor()
}

func (Sandbox_Image) Type() protoreflect.EnumType {
	return &file_sandbox_proto_enumTypes[1]
}

func (x Sandbox_Image) Number() protoreflect.EnumNumber {
	return protoreflect.EnumNumber(x)
}

// Deprecated: Use Sandbox_Image.Descriptor instead.
func (Sandbox_Image) EnumDescriptor() ([]byte, []int) {
	return file_sandbox_proto_rawDescGZIP(), []int{0, 1}
}

type Sandbox struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	// Sandbox unique ID
	Id     int32           `protobuf:"varint,1,opt,name=id,proto3" json:"id,omitempty"`
	Action Sandbox_Action  `protobuf:"varint,2,opt,name=action,proto3,enum=octopulse.Sandbox_Action" json:"action,omitempty"`
	Images []Sandbox_Image `protobuf:"varint,3,rep,packed,name=images,proto3,enum=octopulse.Sandbox_Image" json:"images,omitempty"`
}

func (x *Sandbox) Reset() {
	*x = Sandbox{}
	if protoimpl.UnsafeEnabled {
		mi := &file_sandbox_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *Sandbox) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*Sandbox) ProtoMessage() {}

func (x *Sandbox) ProtoReflect() protoreflect.Message {
	mi := &file_sandbox_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use Sandbox.ProtoReflect.Descriptor instead.
func (*Sandbox) Descriptor() ([]byte, []int) {
	return file_sandbox_proto_rawDescGZIP(), []int{0}
}

func (x *Sandbox) GetId() int32 {
	if x != nil {
		return x.Id
	}
	return 0
}

func (x *Sandbox) GetAction() Sandbox_Action {
	if x != nil {
		return x.Action
	}
	return Sandbox_DEPLOY
}

func (x *Sandbox) GetImages() []Sandbox_Image {
	if x != nil {
		return x.Images
	}
	return nil
}

var File_sandbox_proto protoreflect.FileDescriptor

var file_sandbox_proto_rawDesc = []byte{
	0x0a, 0x0d, 0x73, 0x61, 0x6e, 0x64, 0x62, 0x6f, 0x78, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12,
	0x09, 0x6f, 0x63, 0x74, 0x6f, 0x70, 0x75, 0x6c, 0x73, 0x65, 0x22, 0x8c, 0x02, 0x0a, 0x07, 0x53,
	0x61, 0x6e, 0x64, 0x62, 0x6f, 0x78, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x05, 0x52, 0x02, 0x69, 0x64, 0x12, 0x31, 0x0a, 0x06, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e,
	0x18, 0x02, 0x20, 0x01, 0x28, 0x0e, 0x32, 0x19, 0x2e, 0x6f, 0x63, 0x74, 0x6f, 0x70, 0x75, 0x6c,
	0x73, 0x65, 0x2e, 0x53, 0x61, 0x6e, 0x64, 0x62, 0x6f, 0x78, 0x2e, 0x41, 0x63, 0x74, 0x69, 0x6f,
	0x6e, 0x52, 0x06, 0x61, 0x63, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x30, 0x0a, 0x06, 0x69, 0x6d, 0x61,
	0x67, 0x65, 0x73, 0x18, 0x03, 0x20, 0x03, 0x28, 0x0e, 0x32, 0x18, 0x2e, 0x6f, 0x63, 0x74, 0x6f,
	0x70, 0x75, 0x6c, 0x73, 0x65, 0x2e, 0x53, 0x61, 0x6e, 0x64, 0x62, 0x6f, 0x78, 0x2e, 0x49, 0x6d,
	0x61, 0x67, 0x65, 0x52, 0x06, 0x69, 0x6d, 0x61, 0x67, 0x65, 0x73, 0x22, 0x2b, 0x0a, 0x06, 0x41,
	0x63, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x0a, 0x0a, 0x06, 0x44, 0x45, 0x50, 0x4c, 0x4f, 0x59, 0x10,
	0x00, 0x12, 0x0a, 0x0a, 0x06, 0x44, 0x45, 0x4c, 0x45, 0x54, 0x45, 0x10, 0x01, 0x12, 0x09, 0x0a,
	0x05, 0x52, 0x45, 0x53, 0x45, 0x54, 0x10, 0x02, 0x22, 0x5f, 0x0a, 0x05, 0x49, 0x6d, 0x61, 0x67,
	0x65, 0x12, 0x0b, 0x0a, 0x07, 0x55, 0x4e, 0x4b, 0x4e, 0x4f, 0x57, 0x4e, 0x10, 0x00, 0x12, 0x10,
	0x0a, 0x0c, 0x4d, 0x59, 0x53, 0x51, 0x4c, 0x5f, 0x38, 0x5f, 0x30, 0x5f, 0x31, 0x39, 0x10, 0x01,
	0x12, 0x10, 0x0a, 0x0c, 0x4d, 0x59, 0x53, 0x51, 0x4c, 0x5f, 0x35, 0x5f, 0x37, 0x5f, 0x32, 0x39,
	0x10, 0x02, 0x12, 0x10, 0x0a, 0x0c, 0x4d, 0x59, 0x53, 0x51, 0x4c, 0x5f, 0x35, 0x5f, 0x36, 0x5f,
	0x34, 0x37, 0x10, 0x03, 0x12, 0x13, 0x0a, 0x0f, 0x57, 0x4f, 0x52, 0x44, 0x50, 0x52, 0x45, 0x53,
	0x53, 0x5f, 0x35, 0x5f, 0x33, 0x5f, 0x32, 0x10, 0x04, 0x42, 0x04, 0x5a, 0x02, 0x2e, 0x2f, 0x62,
	0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_sandbox_proto_rawDescOnce sync.Once
	file_sandbox_proto_rawDescData = file_sandbox_proto_rawDesc
)

func file_sandbox_proto_rawDescGZIP() []byte {
	file_sandbox_proto_rawDescOnce.Do(func() {
		file_sandbox_proto_rawDescData = protoimpl.X.CompressGZIP(file_sandbox_proto_rawDescData)
	})
	return file_sandbox_proto_rawDescData
}

var file_sandbox_proto_enumTypes = make([]protoimpl.EnumInfo, 2)
var file_sandbox_proto_msgTypes = make([]protoimpl.MessageInfo, 1)
var file_sandbox_proto_goTypes = []interface{}{
	(Sandbox_Action)(0), // 0: octopulse.Sandbox.Action
	(Sandbox_Image)(0),  // 1: octopulse.Sandbox.Image
	(*Sandbox)(nil),     // 2: octopulse.Sandbox
}
var file_sandbox_proto_depIdxs = []int32{
	0, // 0: octopulse.Sandbox.action:type_name -> octopulse.Sandbox.Action
	1, // 1: octopulse.Sandbox.images:type_name -> octopulse.Sandbox.Image
	2, // [2:2] is the sub-list for method output_type
	2, // [2:2] is the sub-list for method input_type
	2, // [2:2] is the sub-list for extension type_name
	2, // [2:2] is the sub-list for extension extendee
	0, // [0:2] is the sub-list for field type_name
}

func init() { file_sandbox_proto_init() }
func file_sandbox_proto_init() {
	if File_sandbox_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_sandbox_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*Sandbox); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_sandbox_proto_rawDesc,
			NumEnums:      2,
			NumMessages:   1,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_sandbox_proto_goTypes,
		DependencyIndexes: file_sandbox_proto_depIdxs,
		EnumInfos:         file_sandbox_proto_enumTypes,
		MessageInfos:      file_sandbox_proto_msgTypes,
	}.Build()
	File_sandbox_proto = out.File
	file_sandbox_proto_rawDesc = nil
	file_sandbox_proto_goTypes = nil
	file_sandbox_proto_depIdxs = nil
}
