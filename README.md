# Octopulse Protocol Buffers

## Setup

Install the required dependencies by running `yarn` in your shell.

## Generate npm package locally

Simply run `yarn build && yarn pack` in your shell.

## Generate

protoc --proto_path=src/ --go_out=golang/ --js_out=import_style=commonjs,binary:js --python_out=python/protos src/*.proto

## protoc version

libprotoc 3.16.0
